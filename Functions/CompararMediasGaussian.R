#' Compara dados de uma especie
#'
#' @param dadosEspecie 
#'
#' @return
#' @export
#'
#' @examples
CompararMediasGaussian <- function(dados){
  
  # Variaveis para comparacao
  variaveis <- dados %>% 
    select(-Local) %>% 
    colnames()
  
  # Cria modelos
  modelos <- variaveis %>% 
    lapply(
      FUN = function(variavel){
        
        # Exibe processamento
        Msg(
          yellow("Comparando borda com interior da variavel: "), 
          bold(yellow(variavel))
        )
        
        # Cria modelo
        modelo <- bayes.t.test(
          x = dados %>% 
            filter(
              (Local == "Borda")
            ) %>% 
            pull(!!variavel),
          y = dados %>% 
            filter(
              (Local == "Interior")
            ) %>% 
            pull(!!variavel),
          paired = FALSE
        )
        
        # Nomes
        modelo$x_name <- str_c(variavel, " na Borda")
        modelo$y_name <- str_c(variavel, " no Interior")
        
        # Retorno da funcao
        return(modelo)
      }
    )
  
  # Nome dos modelos 
  names(modelos) <- variaveis
  
  # Retorno da funcao
  return(modelos)
}
####
## Fim
#