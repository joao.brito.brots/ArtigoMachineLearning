# Instruções para execução

## Dependências

* Instalar previamente JAGS: Just Another Gibbs Sampler:     https://sourceforge.net/projects/mcmc-jags/files/JAGS/4.x/Windows/

## Execução

* Abrir o projeto ArtigoMachineLearning.Rproj no RStudio
* No RStudio abrir o arquivo _Main.R
* Executar todo o _Main.R
* Resultados gerados no Results/ Resultados.html

## Gitlab

git@gitlab.com:joao.brito.brots/ArtigoMachineLearning.git