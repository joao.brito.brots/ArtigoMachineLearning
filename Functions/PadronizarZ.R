#' Padronizacao em Z centrado em 0 e em escala de desvio padrao
#'
#' @param vetor 
#'
#' @return
#' @export
#'
#' @examples
PadronizarZ <- function(vetor){
  
  # Padronizacao em Z
  vetorPadronizado <- ((vetor - mean(vetor, na.rm = TRUE)) / sd(vetor, na.rm = TRUE))
  
  # Retorno da funcao
  return(vetorPadronizado)
}
####
## Fim
#